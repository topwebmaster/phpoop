<?php
header("Content-Type: text/html; charset=utf-8");
/* Лабораторная работа 1.2
 *  Создание конструктора  и деструктора
 *   *создать класс User
 *   *в классе описать контсруктор который инициализирует параметры name, password, email
 *   *создать деструктор
 *   *деструктор должен выводить строку Пользователь[логин пользователя] удалён.
 *   *
 */
class User{
  public $name;
  public $password;
  public $email;
  function __construct($name, $password, $email) {
    $this->name = $name;
    $this->email = $email;
    $this->password = $password;
    echo "</br>Cоздан пользователь $name";
    }
    function __destruct() {
      echo "</br>Пользователь ".$this->name." удалён";
    }
}
$user1 = new User("Шри Рамачандра", "dhrma", "ramaloka@gmail.com");
$user2 = new User("Рамачандра", "dhrma", "ramaloka@gmail.com");