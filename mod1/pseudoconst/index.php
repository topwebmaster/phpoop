<?php
namespace Yo;
header("Content-Type: text/html; charset=utf-8");//сказали серверу в какой кодировке отображать документ

/**
 * Created by PhpStorm.
 * User: topwebmasterblog@gmail.com
 * Date: 23.06.14
 * Time: 11:38
 * ссылки по теме:
 * http://php.net/manual/ru/language.constants.predefined.php
 *
 */
//Волшебные константы......,,/////////////////////
//описание класса

class Myclass{
    public $name;
    public $email = "email@gmail.com";
    public function UserData($site){//cоздание метода
        echo $site;
    }
    public function getLine(){
        echo "</br>Номер строки".__LINE__;
    }
    public function getFile(){
        echo "</br>Путь к файлу ".__FILE__;
    }
    public function getDir(){
        echo "</br>Директория файла ".__DIR__;
    }
    
}
//Инициализация класса или создание объекта или экземпляра класса
$myclass = new Myclass();

$name=new Myclass();
$name->name = "Krishna";
echo $name->name."</br>";//доступ к свойствам объекта
$name->email = "108@gmail.com";//смена или задания нового свойства
echo $name->email;
$name->getLine();
$name->getFile();
$name->getDir();
