<?php
//http://codomaza.com/article/destruktor-klassa-v-php
//http://www.php.net/manual/ru/language.oop5.decon.php
header("Content-Type: text/html; charset=utf-8");//сказали серверу в какой кодировке отображать документ

class MyClass{
    public $Name2;
    public $Email = "email@gmail.com";
    public function userData($site){//cоздание метода
        echo $site;
    }
    public function getLine(){
        echo "</br>Номер строки".__LINE__;
    }
    public function getFile(){
        echo "</br>Путь к файлу ".__FILE__;
    }
    public function getDir(){
        echo "</br>Директория файла ".__DIR__;
    }
    
    public function __construct1($rt) {
      print "Вывод консруктора $rt </br>";
    }
}
//Инициализация класса или создание объекта или экземпляра класса
$myclass = new MyClass("Конструктор</br>");
$name=new MyClass();
$name->Name2 = "Шри Кришна";
echo $name->Name2."</br>";//доступ к свойствам объекта
$name->Email = "108@gmail.com";//смена или задания нового свойства
echo $name->Email;
$name->getLine();
$name->getFile();



